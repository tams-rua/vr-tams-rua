! function (t) {
    var e = t({});
    t.subscribe = function () {
        e.on.apply(e, arguments)
    }, t.unsubscribe = function () {
        e.off.apply(e, arguments)
    }, t.publish = function () {
        e.trigger.apply(e, arguments)
    }
}(jQuery);
var site = document.querySelector("meta[name=site]").getAttribute("content");
window.api = {
    getLocations: function (t, e) {
        $.post(window.ajaxurl, {
            site: site,
            sheet: t,
            action: "locations"
        }, function (t) {
            "function" == typeof e && e(t)
        })
    },
    addToMailchimpList: function (t, e) {
        $.post(window.ajaxurl, {
            site: site,
            email: t,
            action: "newsletter"
        }, function (t) {
            "function" == typeof e && e(t)
        })
    },
    filter: function (t, e, a, i, n) {
        t.find(".loading__overlay").show();
        var o = t.find(".more");
        $.post(window.ajaxurl, {
            action: "filter",
            sheet: t.data("sheet"),
            view: t.data("view"),
            site: site,
            filters: e,
            offset: a
        }, function (e) {
            t.find(".loading__overlay").hide(), t.find(".list_items__wrapper,.cards__wrapper").tmpl(t.data("template"), e.data, i), e.offset ? (o.show(), o.find("a").data("offset", e.offset)) : o.hide(), $(".audioplayer audio").each(function (t, e) {
                e.addEventListener("ended", function () {
                    $(e).closest(".audioplayer");
                    parent.find(".audio__play").css("display", "inline-block"), parent.find(".audio__pause").css("display", "none")
                })
            }), $.publish("items/loaded", {
                data: e.data
            }), "function" == typeof n && n(e)
        })
    },
    vote: function (t) {
        $.post(window.ajaxurl, {
            action: "upvote",
            site: site,
            vote: t
        })
    }
}, window.helpers = {
    getUrlParameterByName: function (t, e) {
        e || (e = window.location.href), t = t.replace(/[\[\]]/g, "\\$&");
        var a = new RegExp("[?&]" + t + "(=([^&#]*)|&|#|$)"),
            i = a.exec(e);
        return i ? i[2] ? decodeURIComponent(i[2].replace(/\+/g, " ")) : "" : null
    },
    getFiltersFromUrl: function (t) {
        var e = helpers.getUrlParameterByName(t);
        return e ? e.split(",") : ["all"]
    },
    getActiveFilters: function (t) {
        var e = [];
        return t.find(".filter__item.active").each(function (t, a) {
            e.push(encodeURIComponent($(a).attr("data-tag")))
        }), e.length || (e = ["all"]), e
    }
}, $(document).ready(function () {
    $.fn.tmpl = function (t, e, a) {
        var i = doT.template($("#" + t).html());
        return $.isArray(e) || (e = [e]), this.each(function () {
            for (var t = "", n = 0; n < e.length; n++) t += i(e[n]);
            a ? $(this).append(t) : $(this).html(t)
        })
    }, $(".nav__toggle,.nav__backdrop").on("click", function (t) {
        t.preventDefault(), $(".nav__mobile").toggleClass("active"), $(".nav__backdrop").fadeToggle(120)
    }), $(".nav__mobile a").on("click", function () {
        $(".nav__mobile").removeClass("active"), $(".nav__backdrop").fadeOut(120)
    }), $('a[href^="#"]').each(function (t, e) {
        var a = $(e);
        a.on("click", function (t) {
            t.preventDefault();
            var e = a.attr("href");
            if ("#" !== e) {
                var i = $('a[name="' + e.replace("#", "") + '"]').offset();
                $("html,body").animate({
                    scrollTop: i.top - 20,
                    scrollLeft: i.left
                })
            }
        })
    }), $(".list,.cards").each(function (t, e) {
        var a = $(e),
            i = a.find(".filters li"),
            n = a.data("filter-type"),
            o = i.filter("[data-tag=all]"),
            r = a.data("sheet"),
            l = ["all"];
        l = helpers.getUrlParameterByName(r) ? helpers.getFiltersFromUrl(r) : helpers.getActiveFilters(a), l.forEach(function (t) {
            i.filter('[data-tag="' + t + '"]').addClass("active")
        }), api.filter(a, l), i.on("click", function () {
            "all" !== $(this).data("tag") && "or" !== n && "select one" !== n || i.removeClass("active"), $(this).toggleClass("active"), i.not(o).filter(".active").length > 0 ? o.removeClass("active") : o.addClass("active");
            var t = helpers.getActiveFilters(a);
            api.filter(a, t)
        }), a.find(".more a ").on("click", function (t) {
            t.preventDefault();
            var e = helpers.getActiveFilters(a);
            api.filter(a, e, $(this).data("offset"), !0)
        }), a.on("click", ".upvote", function (t) {
            var e = $(this).data("vote");
            if (localStorage.getItem(e)) return !1;
            var a = $(this),
                i = a.find(".voting");
            api.vote(e), localStorage.setItem(e, "1"), i.html(a.data("votes") + 1)
        })
    }), $(".list_items__wrapper").on("click", ".list_item.has_detail .list_item__row", function (t) {
        t.preventDefault();
        var e = $(this).closest(".list_item");
        e.toggleClass("is_open"), e.find(".list_item__detail").stop(!0, !0).slideToggle(150)
    }), $(".mapbox-map").each(function (t, e) {
        var a = $(e),
            i = mapboxMaps[a.attr("id")],
            n = a.data("locations");
        "object" == typeof n ? n.forEach(function (t) {
            var e = [t[1], t[0]],
                n = document.createElement("div");
            n.className = "marker", n.style.background = a.data("marker"), new mapboxgl.Marker(n).setLngLat(e).addTo(i)
        }) : api.getLocations(n, function (t) {
            a.data("has-center") || i.setCenter([t.data[0].lng, t.data[0].lat]), t.data.forEach(function (t) {
                var e = [t.lng, t.lat],
                    n = document.createElement("div");
                n.className = "marker", n.style.background = a.data("marker");
                var o = new mapboxgl.Marker(n).setLngLat(e);
                if (t.title || t.description || t.image) {
                    var r = "";
                    t.title && (r += "<h1>" + t.title + "</h1>"), t.image && (r += '<img src="' + t.image + '" />'), t.description && (r += "<p>" + t.description + "</p>");
                    var l = new mapboxgl.Popup({
                        offset: 15,
                        anchor: ""
                    }).setMaxWidth("300px").setHTML(r).setLngLat(e).addTo(i);
                    o.setPopup(l)
                }
                o.addTo(i)
            })
        })
    }), $(".btn-subscribe-newsletter").each(function (t, e) {
        var a = $(e);
        a.on("click", function (t) {
            t.preventDefault();
            var e = a.closest(".input-group"),
                i = e.find("input[type=email]").val();
            i && (e.find(".subscriber-add").html("&hellip;"), api.addToMailchimpList(i, function () {
                e.find(".subscriber-add").hide(), e.find(".subscriber-added").show()
            }))
        })
    })
});